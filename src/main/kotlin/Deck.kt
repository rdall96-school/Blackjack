/*
 * Deck.kt
 * Project: Blackjack
 *
 * Author: Ricky Dall'Armellina
 * 2019-03-07
 *
 * Copyright © 2019 Ricky Dall'Armellina. All rights reserved.
 * 
 */

class Deck(numberOfDecks: Int) {

    // Data
    var cards: MutableList<Card> = ArrayList()


    // Initializer

    init {
        // Check caps of numberOfDecks [1, 8]
        val amount: Int = when {
            numberOfDecks > 8 -> 8
            numberOfDecks < 1 -> 1
            else -> numberOfDecks
        }

        // For every deck generate a set and add it to the cards list
        for(decks in 1..amount) {
            this.cards.addAll(generateDeck())
        }

        shuffleDeck()
    }


    // Methods

    private fun generateDeck(): MutableList<Card> {
        val suits = arrayOf("Clubs", "Diamonds", "Hearts", "Spades")
        val faces = arrayOf("Ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King")
        val cards: MutableList<Card> = ArrayList()
        for(suit in suits) {
            for(face in faces) {
                cards.add(Card(suit, face))
            }
        }
        return cards
    }

    fun shuffleDeck() { this.cards.shuffle() }

    fun draw(amount: Int): MutableList<Card> {
        val cards_drew: MutableList<Card> = arrayListOf()
        for(item in 1..amount) {
            checkEmpty()
            cards_drew.add(cards.last())
            cards.remove(cards.last())
        }
        return cards_drew
    }

    fun checkEmpty() {
        if(cards.isEmpty()) {
            // Regenerate the deck
            this.cards = generateDeck()
            shuffleDeck()
        }
    }

}