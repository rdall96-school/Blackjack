/*
 * Player.kt
 * Project: Blackjack
 *
 * Author: Ricky Dall'Armellina
 * 2019-03-13
 *
 * Copyright © 2019 Ricky Dall'Armellina. All rights reserved.
 * 
 */

open class Player() {

    // Data

    open var name: String = "PLAYER"
    var money: Int = 1_000
    var bet: Int = 0
    protected var cards: MutableList<Card> = ArrayList()
    var handAmount: Int = 0
    var winCnt: Int = 0


    // Initializer

    constructor(name: String) : this() {
        this.name = name
    }


    // Methods

    fun giveCard(cards: MutableList<Card>) {
        for(card in cards) {
            this.cards.add(card)
        }
        calculateHandAmount()
    }

    fun showCards(): Set<String> {
        return this.cards.groupBy { it.getName() }.keys
    }

    fun removeCards() {
        this.cards.removeAll(this.cards)
    }

    protected fun hasAce(): Boolean {
        for(card in cards) {
            if(card.isAce()) { return true }
        }
        return false
    }

    private fun calculateHandAmount() {
        var amount = 0
        for (card in cards) {
            amount += card.value
        }
        val aceAmount = amount + 10

        handAmount = when (hasAce()) {
            true -> when (aceAmount > 21) {
                true -> amount
                false -> aceAmount
            }
            false -> amount
        }
    }

    fun hasMoney(): Boolean {
        return (this.money > 0)
    }

    fun wonBet() {
        money += bet
        bet = 0
        winCnt += 1
    }

    fun lostBet() {
        money -= bet
        bet = 0
    }

    fun makeBet(bet: Int) {
        this.bet = when {
            bet > 20 -> 20
            bet < 5 -> 5
            bet > money -> money
            else -> bet
        }
    }

    fun makeMove(input: String): Boolean {
        return when (input) {
            "h", "H" -> true
            "s", "S" -> false
            else -> false
        }
    }

    fun isBusted(): Boolean {
        return when {
            handAmount <= 21 -> false
            else -> true
        }
    }
}