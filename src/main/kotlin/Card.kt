/*
 * Card.kt
 * Project: Blackjack
 *
 * Author: Ricky Dall'Armellina
 * 2019-03-09
 *
 * Copyright © 2019 Ricky Dall'Armellina. All rights reserved.
 * 
 */

class Card(suit: String, face: String) {

    // Data

    private var suit: String
    private var face: String
    var value: Int
    var alternate_value: Int? = null


    // Initializer

    init {
        this.suit = suit
        this.face = face
        this.value = when(face) {
            "Ace" -> 1
            "Jack" -> 10
            "Queen" -> 10
            "King" -> 10
            else -> face.toInt()
        }
        // When the value is one, card is ace, so alternate value is 11 for blackjack
        if(value == 1) { this.alternate_value = 11 }
    }


    // Methods

    fun getName(): String { return "$face of $suit" }

    fun getFace(): String { return this.face }

    fun isAce(): Boolean {
        return when {
            face == "Ace" -> true
            else -> false
        }
    }

}