/*
 * Dealer.kt
 * Project: Blackjack
 *
 * Author: Ricky Dall'Armellina
 * 2019-03-09
 *
 * Copyright © 2019 Ricky Dall'Armellina. All rights reserved.
 * 
 */

class Dealer() {

    // Data

    var name: String = "DEALER"
    private var money: Int = 0
    private var cards: MutableList<Card> = ArrayList()
    var handAmount: Int = 0
    var winCnt: Int = 0


    // Methods

    fun giveCard(cards: MutableList<Card>) {
        for(card in cards) {
            this.cards.add(card)
        }
    }

    fun showCards(): MutableList<Card> { return this.cards }

    fun removeCards() {
        this.cards.removeAll(this.cards)
    }

    private fun calculateHandAmount(): Int {
        // Calculate regular amount
        var amount = 0
        for (card in cards) {
            amount += card.value
        }
        return amount
    }

    private fun calculateAceHandAmount(): Int {
        // Calculate amount with ace if any
        return calculateHandAmount() + 10
    }

    private fun hasAce(): Boolean {
        for(card in cards) {
            if(card.isAce()) { return true }
        }
        return false
    }

    fun hasBlackjack(): Boolean {
        return when (hasAce()) {
            true -> when {
                cards.size == 2 -> when {
                    calculateAceHandAmount() == 21 -> true
                    else -> false
                }
                else -> false
            }
            else -> false
        }
    }

    fun wonMoney(money: Int) {
        this.money += money
    }

    fun getMoney(): Int { return this.money }

    fun won() {
        winCnt += 1
    }

    fun makeMove(): Boolean {
        // Return 'false' if stands, 'true' is hit

        // Calculate regular amount
        handAmount = calculateHandAmount()
        // Calculate amount with ace if any
        if(hasAce() && calculateAceHandAmount() > handAmount && calculateAceHandAmount() <= 21) {
            handAmount = calculateAceHandAmount()
        }

        // If current amount less than 17, then hit
        return when {
            handAmount < 17 -> true
            else -> false
        }

    }

    fun isBusted(): Boolean {
        return when {
            handAmount <= 21 -> false
            else -> true
        }
    }


}
