import kotlin.random.Random

/*
 * CPUPlayer.kt
 * Project: Blackjack
 *
 * Author: Ricky Dall'Armellina
 * 2019-03-11
 *
 * Copyright © 2019 Ricky Dall'Armellina. All rights reserved.
 * 
 */

class CPUPlayer() : Player() {

    // Data

    override var name: String = "CPU"
    private var beSmart: Boolean = false


    // Initializer

    constructor(name: String, smart: Boolean) : this() {
        this.name = name
        this.beSmart = smart
    }


    // Methods

    private fun calculateHandAmount(): Int {
        // Calculate regular amount
        var amount = 0
        for (card in cards) {
            amount += card.value
        }
        return amount
    }

    private fun calculateAceHandAmount(): Int {
        // Calculate amount with ace if any
        return calculateHandAmount() + 10
    }

    private fun getNonAceCardValue(): Int {
        return cards.maxBy { it.value }!!.value
    }

    fun makeBet() {
        bet = when(beSmart) {
            true && money >= 1000 -> Random.nextInt(15, 20)
            true && money < 1000 -> Random.nextInt(5, 8)
            false && money >= 1000 -> Random.nextInt(12, 20)
            false && money < 1000 -> Random.nextInt(5, 12)
            else -> Random.nextInt(5, 12)
        }
        if(bet > money) { bet = money }
    }

    fun makeMove(dealerFirstCard: Card): Boolean {
        return when (beSmart) {
            true -> makeSmartMove(dealerFirstCard)
            false -> makeRegularMove()
        }
    }

    private fun makeRegularMove(): Boolean {
        // Return 'false' if stands, 'true' is hit

        // Calculate regular amount
        handAmount = calculateHandAmount()
        // Calculate amount with ace if any
        if(hasAce() && calculateAceHandAmount() > handAmount && calculateAceHandAmount() <= 21) {
            handAmount = calculateAceHandAmount()
        }

        // If current amount less than 15, then hit
        return when {
            handAmount < 15 -> true
            else -> false
        }

    }

    private fun makeSmartMove(dealerFirstCard: Card): Boolean {

        // Calculate current handAMount
        handAmount = when (hasAce()) {
            true -> when (calculateAceHandAmount() > 21) {
                true -> calculateHandAmount()
                false -> calculateAceHandAmount()
            }
            false -> calculateHandAmount()
        }

        // Return 'false' if stands, 'true' if hit
        return when (cards.count() == 2) {
            // Only the first two cards
            true -> when (hasAce()) {
                // Has ace, use soft_totals
                true -> when {
                    // Other card is high
                    getNonAceCardValue() > 9 -> false
                    getNonAceCardValue() < 2 -> true
                    // has ace so use soft_totals table to get value for hit or stand
                    else -> soft_totals.getValue(dealerFirstCard.getFace()).getValue(getNonAceCardValue())
                }
                // No ace, use hard_totals
                false -> when {
                    handAmount > 17 -> false
                    handAmount < 8 -> true
                    else -> hard_totals.getValue(dealerFirstCard.getFace()).getValue(calculateHandAmount())
                }
            }
            // More than the first two cards, need to use hard_totals
            false -> when {
                handAmount > 17 -> false
                calculateHandAmount() < 8 -> true
                else -> hard_totals.getValue(dealerFirstCard.getFace()).getValue(calculateHandAmount())
            }
        }
    }


}

// Tables for a more intelligent player that plays according to standard Blackjack strategies
// Stands: false, Hit: true. These are the returns for the function makeMove()
const val S: Boolean = false
const val H: Boolean  = true
// A soft total is any hand that has an Ace as one of the first two cards, the ace counts as 11 to start.
val soft_totals: Map<String, Map<Int, Boolean>> = mapOf(
    "2" to mapOf(9 to S, 8 to S, 7 to S, 6 to H, 5 to H, 4 to H, 3 to H, 2 to H),
    "3" to mapOf(9 to S, 8 to S, 7 to S, 6 to H, 5 to H, 4 to H, 3 to H, 2 to H),
    "4" to mapOf(9 to S, 8 to S, 7 to S, 6 to H, 5 to H, 4 to H, 3 to H, 2 to H),
    "5" to mapOf(9 to S, 8 to S, 7 to S, 6 to H, 5 to H, 4 to H, 3 to H, 2 to H),
    "6" to mapOf(9 to S, 8 to S, 7 to S, 6 to H, 5 to H, 4 to H, 3 to H, 2 to H),
    "7" to mapOf(9 to S, 8 to S, 7 to S, 6 to H, 5 to H, 4 to H, 3 to H, 2 to H),
    "8" to mapOf(9 to S, 8 to S, 7 to S, 6 to H, 5 to H, 4 to H, 3 to H, 2 to H),
    "9" to mapOf(9 to S, 8 to S, 7 to H, 6 to H, 5 to H, 4 to H, 3 to H, 2 to H),
    "10" to mapOf(9 to S, 8 to S, 7 to H, 6 to H, 5 to H, 4 to H, 3 to H, 2 to H),
    "Jack" to mapOf(9 to S, 8 to S, 7 to H, 6 to H, 5 to H, 4 to H, 3 to H, 2 to H),
    "Queen" to mapOf(9 to S, 8 to S, 7 to H, 6 to H, 5 to H, 4 to H, 3 to H, 2 to H),
    "King" to mapOf(9 to S, 8 to S, 7 to H, 6 to H, 5 to H, 4 to H, 3 to H, 2 to H),
    "Ace" to mapOf(9 to S, 8 to S, 7 to H, 6 to H, 5 to H, 4 to H, 3 to H, 2 to H)
)
// A hard total is any hand that does not start with an ace in it, or it has been dealt an ace that can only be counted as 1 instead of 11.
val hard_totals: Map<String, Map<Int, Boolean>> = mapOf(
    "2" to mapOf(17 to S, 16 to S, 15 to S, 14 to S, 13 to S, 12 to H, 11 to H, 10 to H, 9 to H, 8 to H),
    "3" to mapOf(17 to S, 16 to S, 15 to S, 14 to S, 13 to S, 12 to H, 11 to H, 10 to H, 9 to H, 8 to H),
    "4" to mapOf(17 to S, 16 to S, 15 to S, 14 to S, 13 to S, 12 to S, 11 to H, 10 to H, 9 to H, 8 to H),
    "5" to mapOf(17 to S, 16 to S, 15 to S, 14 to S, 13 to S, 12 to S, 11 to H, 10 to H, 9 to H, 8 to H),
    "6" to mapOf(17 to S, 16 to S, 15 to S, 14 to S, 13 to S, 12 to S, 11 to H, 10 to H, 9 to H, 8 to H),
    "7" to mapOf(17 to S, 16 to H, 15 to H, 14 to H, 13 to H, 12 to H, 11 to H, 10 to H, 9 to H, 8 to H),
    "8" to mapOf(17 to S, 16 to H, 15 to H, 14 to H, 13 to H, 12 to H, 11 to H, 10 to H, 9 to H, 8 to H),
    "9" to mapOf(17 to S, 16 to H, 15 to H, 14 to H, 13 to H, 12 to H, 11 to H, 10 to H, 9 to H, 8 to H),
    "10" to mapOf(17 to S, 16 to H, 15 to H, 14 to H, 13 to H, 12 to H, 11 to H, 10 to H, 9 to H, 8 to H),
    "Jack" to mapOf(17 to S, 16 to H, 15 to H, 14 to H, 13 to H, 12 to H, 11 to H, 10 to H, 9 to H, 8 to H),
    "Queen" to mapOf(17 to S, 16 to H, 15 to H, 14 to H, 13 to H, 12 to H, 11 to H, 10 to H, 9 to H, 8 to H),
    "King" to mapOf(17 to S, 16 to H, 15 to H, 14 to H, 13 to H, 12 to H, 11 to H, 10 to H, 9 to H, 8 to H),
    "Ace" to mapOf(17 to S, 16 to H, 15 to H, 14 to H, 13 to H, 12 to H, 11 to H, 10 to H, 9 to H, 8 to H)
)
