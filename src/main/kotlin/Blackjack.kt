/*
 * Blackjack.kt
 * Project: Blackjack
 *
 * Author: Ricky Dall'Armellina
 * 2019-03-07
 *
 * Copyright © 2019 Ricky Dall'Armellina. All rights reserved.
 * 
 */

fun main() {

    print("\nWELCOME TO BLACKJACK!\n")
    println(
        "Select one of the following options: \n" +
                "\t1.Play\n" +
                "\t2.CPU game demo"
    )
    var isValid = true
    while (isValid) {
        when (readLine()!!.toInt()) {
            1 -> {
                isValid = false
                play()
            }
            2 -> {
                isValid = false
                cpu_demo()

            }
            else -> println(
                "Please enter a valid option: \n" +
                        "\t1.Play\n" +
                        "\t2.CPU game demo"
            )
        }
    }
}



fun play() {

    LOG("\n - BLACKJACK - \n")

    // Setup deck
    print("How many decks would you like to play with? (default: 6, choose between 1-8) ")
    var deckCount: String? = "6"
    var isDeckCountValid = false
    while(!isDeckCountValid) {
        deckCount = readLine()
        when(deckCount) {
            null, "" -> {
                deckCount = "6"
                isDeckCountValid = true
            }
            "1", "2", "3", "4", "5", "6", "7", "8" -> {
                isDeckCountValid = true
            }
            else -> println("Please insert a valid amount (1-8): ")
        }
    }
    val deck = Deck(deckCount!!.toInt())

    // Setup players
    val dealer = Dealer()
    val players: MutableList<Player> = mutableListOf()

    var isDoneAddingPlayers: Boolean
    do {

        println("Player #${players.size + 1} name: [PLAYER_${players.count() + 1}] ")
        val playerName: String? = readLine().toString()
        when (playerName) {
            null -> players.add(Player("PLAYER_${players.count() + 1}"))
            else -> players.add(Player(playerName))
        }

        println("Want to add another player? [y/N] ")
        isDoneAddingPlayers = when(readLine().toString()) {
            "y", "Y", "YES", "yes", "Yes" -> true
            else -> false
        }

    } while(isDoneAddingPlayers)

    println("How many CPU players would you like to add? [default: 0] ")
    var cpuPlayerCount: String? = readLine() ?: "0"
    if(cpuPlayerCount == "") { cpuPlayerCount = "0" }
    when {
        cpuPlayerCount != null && cpuPlayerCount.toInt() > 0 -> {
            for(item in 1..cpuPlayerCount.toInt()) {
                players.add(CPUPlayer("CPU_$item", true))
            }
        }
    }

    var isGameRunning = true

    while(isGameRunning) {

        // DRAW CARDS
        println("Giving each player 2 cards...")
        for(player in players) {
            player.giveCard(deck.draw(2))
        }
        dealer.giveCard(deck.draw(2))

        // BETS
        println("\nPlace your bets players")
        for(player in players) {
            when(player) {
                is CPUPlayer -> player.makeBet()
                else -> {
                    do {
                        println("${player.name}, how much do you wanna bet? ($5-20 allowed)\n\t[current available amount: $${player.money}] ")
                        val bet: String? = readLine() ?: ""
                        if (bet == "" || bet == null) { println("\tInvalid amount") }
                        else { player.makeBet(bet.toInt()) }
                    } while(bet == "" || bet == null)
                }
            }
        }
        println("\n- BETS -")
        for(player in players) {
            println("\t${player.name} bet: ${player.bet}")
        }

        // MOVES
        println("\n${dealer.name}'s first card: ${dealer.showCards().first().getName()}")

        // If dealer has Blackjack, then it won
        if(dealer.hasBlackjack()) {

            println("${dealer.name} has Blackjack. ${dealer.name} wins!")
            for(player in players) {
                dealer.wonMoney(player.bet)
                player.lostBet()
            }

        }
        else {
            // Give cards to players until they stand
            for (player in players) {
                when (player) {
                    is CPUPlayer -> while (player.makeMove(dealer.showCards().first())) {
                        player.giveCard(deck.draw(1))
                    }
                    else -> {
                        println("\n${player.name}")
                        do {
                            println("\tYour cards are: ${player.showCards()}")
                            println("\tYour current highest hand amount is: ${player.handAmount}")
                            println("\tHit or Stand? [h/S] ")
                            var input: Boolean = player.makeMove(readLine().toString())
                            when(input) { true -> player.giveCard(deck.draw(1)) }
                            // Check if player is busted
                            when(player.isBusted()) {
                                true -> {
                                    println("\tYour current hand amount is: ${player.handAmount}. You're busted!")
                                    input = false
                                }
                            }
                        } while (input)
                    }
                }
            }

            // Give cards to dealer until he stands
            while(dealer.makeMove()) {
                dealer.giveCard(deck.draw(1))
            }

            // SCORE
            println("\n- SCORE -")
            val winners: Set<String>? = pickWinner(players, dealer)
            if(winners == null) { println("\tNo winner") }
            else {
                for(winner in winners) {
                    println("\t$winner won!")
                    if (winner == dealer.name) {
                        dealer.won()
                    }
                }

                // Add subtracts bets
                for (player in players) when (player.name) {
                    in winners -> player.wonBet()
                    else -> {
                        dealer.wonMoney(player.bet)
                        player.lostBet()
                    }
                }
            }
        }

        // RESET
        dealer.removeCards()
        for(player in players) { player.removeCards() }

        // Check if players still have money to play
        for(player in players) {
            if (!player.hasMoney()) {
                println("${player.name} has no money left.")
                players.remove(player)
            }
        }

        println("\nPlay again? [Y/n] ")
        isGameRunning = when(readLine().toString()) {
            "n", "N", "No", "NO", "no" -> false
            else -> true
        }

    }

    // Game over, print game stats
    printGameStats(players, dealer)

}



fun cpu_demo() {

    LOG("\n - BLACKJACK CPU DEMO - \n")

    // Setup deck
    val deck = Deck(6)

    // Setup CPU players
    val dealer = Dealer()
    val cpuPlayers: MutableList<CPUPlayer> = arrayListOf(
        CPUPlayer("CPU_1", false),
        CPUPlayer("CPU_2", false),
        CPUPlayer("CPU_3", true)
    )

    var round = 100

    while (round > 0) {

        LOG("- BETS -")
        for(player in cpuPlayers) {
            // Give two cards to every player
            player.giveCard(deck.draw(2))
            // Player makes bet
            player.makeBet()
            LOG("\t${player.name} bet: ${player.bet}")
        }

        // Give dealer two cards
        dealer.giveCard(deck.draw(2))
        LOG("${dealer.name} first card: ${dealer.showCards().first().getName()}")

        // If dealer has Blackjack, then it won
        if(dealer.hasBlackjack()) {

            LOG("${dealer.name} has Blackjack. ${dealer.name} wins!")
            for(player in cpuPlayers) {
                dealer.wonMoney(player.bet)
                player.lostBet()
            }

        }
        else {

            // Keep giving cards until player stands
            for(player in cpuPlayers) {
                while (player.makeMove(dealer.showCards().first())) {
                    player.giveCard(deck.draw(1))
                }
            }

            // Give cards to dealer until he stands
            while(dealer.makeMove()) {
                dealer.giveCard(deck.draw(1))
            }

            // Dealer stands, count final amounts
            LOG("- SCORES -")
            LOG("\t${dealer.name}: ${dealer.handAmount}")
            for(player in cpuPlayers) {
                LOG("\t${player.name}: ${player.handAmount}")
            }

            // Get winner
            val winners: Set<String>? = pickWinner(cpuPlayers as MutableList<Player>, dealer)
            if(winners == null) { LOG("No winner") }
            else {
                for(winner in winners) {
                    LOG("$winner won!")
                    if (winner == dealer.name) {
                        dealer.won()
                    }
                }

                // Add subtracts bets
                for (player in cpuPlayers) when (player.name) {
                    in winners -> player.wonBet()
                    else -> {
                        dealer.wonMoney(player.bet)
                        player.lostBet()
                    }
                }
            }

        }

        // remove cards from players
        dealer.removeCards()
        for(player in cpuPlayers) { player.removeCards() }

        // Check if players still have money to play
        for(player in cpuPlayers) {
            if (!player.hasMoney()) {
                LOG("${player.name} has no money left.")
                round = 0
                break
            }
        }

        LOG("\n")
        round -= 1
    }

    printGameStats(cpuPlayers as MutableList<Player>, dealer)

}



fun pickWinner(players: MutableList<Player>, dealer: Dealer): Set<String>? {

    // Ordered list of players by current handAmount
    val playerList: MutableList<Player> = arrayListOf()
    for(player in players) when {
        !player.isBusted() -> playerList.add(player)
    }
    // Check for tie
    var prevScore: Int? = (
            when (playerList.isEmpty()) {
                true -> null
                false -> playerList.first().handAmount
            }
            )
    val tiePlayerList: MutableSet<Player> = mutableSetOf()
    for(player in playerList) {
        if(player.handAmount == prevScore) { tiePlayerList.add(player) }
        else { prevScore = player.handAmount }
    }
    val winningPlayer: Player? = playerList.maxBy { it.handAmount }

    // Compare scores
    return when {
        dealer.handAmount == 21 -> setOf(dealer.name)
        !dealer.isBusted() -> when {
            tiePlayerList.count() > 1 && tiePlayerList.first().handAmount > dealer.handAmount -> null
            winningPlayer != null -> when {
                dealer.handAmount >= winningPlayer.handAmount -> setOf(dealer.name)
                else -> setOf(winningPlayer.name)
            }
            else -> setOf(dealer.name)
        }
        dealer.isBusted() -> when (winningPlayer) {
            null -> null
            else -> when (winningPlayer.handAmount) {
                21 -> setOf(winningPlayer.name)
                else -> playerList.groupBy { it.name }.keys
            }
        }
        else -> null
    }
}

fun printGameStats(players: MutableList<Player>, dealer: Dealer) {
    println("\n- GAME STATS -\n")
    println(dealer.name)
    println("\twon: ${dealer.winCnt} time/s")
    println("\tmoney: ${dealer.getMoney()}")
    for(player in players) {
        println(player.name)
        println("\twon: ${player.winCnt} time/s")
        println("\tmoney: ${player.money}")
    }
}

fun LOG(text: String) {
    val VERBOSE = true
    when(VERBOSE) {
        true -> println(text)
    }
}
